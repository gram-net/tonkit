"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MAX_REFS = 4;
const map = new WeakMap;
class CellData {
    constructor(rep, len, ...ref) {
        this.rep = rep || 0n;
        this.len = len || 0n;
        this.ref = ref;
    }
}
class Builder {
    constructor() {
        map.set(this, new CellData);
    }
    int(num, len) {
        if (num < 0) {
        }
        else {
            return this.uint(num, len);
        }
    }
    uint(num, len) {
        const data = map.get(this);
        const mask = (2n ** (len + 1n)) - 1n;
        data.rep = (data.rep << len) + (mask & num);
        data.len += len;
        return this;
    }
    ref(cell) {
        const data = map.get(this);
        if (data.ref.length == MAX_REFS)
            throw new Error('max refs');
        data.ref.push(cell);
        return this;
    }
    end() {
        return new Cell(this);
    }
}
exports.Builder = Builder;
class Cell {
    constructor(builder) {
    }
    get hash() {
        const buffer = new ArrayBuffer(32);
        return buffer;
    }
}
exports.Cell = Cell;
class Slice {
}
exports.Slice = Slice;
//# sourceMappingURL=cell.js.map