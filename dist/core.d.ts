/// <reference types="node" />
import { dirname, join, normalize } from 'path';
import { exec as cp_exec, spawn as cp_spawn } from 'child_process';
import * as fs from 'fs';
export { dirname, join, normalize };
export { cp_exec, cp_spawn };
export declare const fsp: typeof fs.promises;
export declare const DEBUG = 0;
export declare const DEBUG_FIFT: 0;
export declare const DEBUG_FIFT_COMMAND: 0;
export declare const DEBUG_FIFT_OUTPUT: 0;
export declare const PATH_FIFTLIB: string;
export declare const PATH_ROOT: string;
export declare const PATH_FIFT: string;
export declare const PATH_FIXT: string;
export declare const TMPDIR: string;
export declare const FIFTPATH: string;
export declare const log: any;
export declare function toHex(arg: any): string;
export declare function fromHex(str: string): Uint8Array;
export declare function exec(...args: (string | Buffer)[]): Promise<string>;
export declare function handleUnhandledRejections(): void;
export declare class fift {
    fift: any;
    res: any;
    timer: any;
    running: boolean;
    cmdArgs: Array<any>;
    constructor(fift?: any);
    destroy(): void;
    timerFunc(msg: any, timeout?: number): void;
    run(...args: any): Promise<{
        res: any;
        bocBuffer: Buffer;
        sendHashBuffer: Buffer;
        outBuffer: Buffer;
    }>;
}
export declare function until(fn: any, delay?: number, retries?: number): Promise<unknown>;
