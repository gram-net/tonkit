"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapTransactions = response => {
  return response
    .map(t => {
      const inMsg = t.in_message;
      const outMsgs = t.out_messages;
      const transaction = { ...t };
      delete transaction.in_message;
      delete transaction.out_messages;
      const messages = outMsgs.map(m => ({
        ...transaction,
        ...m,
        type: "incoming"
      }));
      if (inMsg) {
        messages.push({
          ...transaction,
          ...inMsg,
          type: "outgoing"
        });
      }
      const myAddr = (hex, xaddr) => {
        const addrBuf = Buffer.from(hex, "hex");
        const xAddrBuf = Buffer.from(xaddr, "base64").slice(2, 34);
        return Buffer.compare(addrBuf, xAddrBuf) === 0;
      };
      const mapped = messages.map(m => {
        if (m.value) {
          m.value = +m.value;
          if (myAddr(m.account_hex, m.destination)) m.type = "received";
          if (myAddr(m.account_hex, m.source)) m.type = "send";
        }
        return m;
      });
      return mapped;
    })
    .flat()
    .slice()
    .sort((a, b) => b.time - a.time);
};
//# sourceMappingURL=transaction.js.map
