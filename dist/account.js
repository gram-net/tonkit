"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
// import { ClientRemote as Client } from './client-remote'
const parse_js_1 = require("./parse.js");
const core_js_1 = require("./core.js");
const nacl = tslib_1.__importStar(require("tweetnacl"));
const uuid_1 = require("uuid");
const bases = new Map();
const clients = new Map();
const FN_HASH = core_js_1.join(core_js_1.PATH_FIXT, "init-wallet.hash");
class Account {
  constructor(base, client, saddr, nbaddr, baddr) {
    this.saddr = saddr;
    this.nbaddr = nbaddr;
    this.baddr = baddr;
    bases.set(this, base);
    clients.set(this, client);
    this.wc = saddr.split(":")[0];
  }
  get base() {
    return bases.get(this);
  }
  get client() {
    return clients.get(this);
  }
  static async create(wc, base, client, script = Account.SCRIPT_NEW) {
    await client.fift.run(script, wc.toString(), base);
    return Account.load(base, client);
  }
  static async createExternal(wc, base, keyPair, client) {
    const hash = await core_js_1.fsp.readFile(FN_HASH);
    await core_js_1.fsp.writeFile(base + ".pk", keyPair.publicKey);
    await core_js_1.fsp.writeFile(
      base + ".sig",
      nacl.sign.detached(hash, keyPair.secretKey)
    );
    await client.fift.run(Account.SCRIPT_NEW_EXT, wc.toString(), base);
    return Account.load(base, client);
  }
  async sleep(ms = 2500) {
    return new Promise(resolve => {
      setTimeout(resolve, ms);
    });
  }
  static async load(base, client) {
    const fiftResult = await client.fift.run(Account.SCRIPT_SHOW_ADDR, base);
    // console.warn(base, client, fiftResult)
    // console.warn(saddr(fiftResult.res), nbaddr(fiftResult.res), baddr(fiftResult.res))
    return new Account(
      base,
      client,
      parse_js_1.saddr(fiftResult.res),
      parse_js_1.nbaddr(fiftResult.res),
      parse_js_1.baddr(fiftResult.res)
    );
  }
  static fromAddressObj(base, addressObj, client) {
    return new Account(
      base,
      client,
      addressObj.saddr,
      addressObj.nbaddr,
      addressObj.baddr
    );
  }
  static fromFiftRes(fiftRes, base, client) {
    return new Account(
      base,
      client,
      parse_js_1.saddr(fiftRes),
      parse_js_1.nbaddr(fiftRes),
      parse_js_1.baddr(fiftRes)
    );
  }
  async sendInitQuery() {
    await this.client.sendFile(this.base + "-query.boc");
    await this.client
      .waitInfoChange(this.saddr, "state", "uninit")
      .catch(e => console.error(e));
  }
  async info() {
    const result = await this.client.getaccount(this.baddr);
    return result;
  }
  async txs() {
    const result = await this.client.txs(this.baddr);
    return result;
  }
  async deploy(deployFile) {
    const result1 = await this.client.last();
    let testgiverAddr = "kf9mZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZnw8";
    let result = await this.client.runmethod(testgiverAddr, "seqno");
    let testgiverSeqNo = Number(result);
    // console.warn("TESTGIVER SEQNO", {testgiverSeqNo})
    const bocPath =
      core_js_1.TMPDIR +
      "/testgiver-zerostate-" +
      uuid_1.v5("testgiver-zerostate-" + testgiverSeqNo, uuid_1.v5.URL);
    const fiftResultB = await this.client.fift.run(
      core_js_1.join(core_js_1.PATH_FIFT, "testgiver-zerostate.fif"),
      this.nbaddr,
      testgiverSeqNo,
      20,
      bocPath
    );
    // console.log(fiftResultB)
    if (this.client.remote) {
      result = await this.client.sendFile(Buffer.from(fiftResultB.bocBuffer));
    } else {
      result = await this.client.sendFile(bocPath + ".boc");
    }
    // console.log("SEND FILE TESTGIVER RESULT",result)
    const { client } = this;
    // console.warn("WAIT FOR SEQUENCE NUMBER")
    return core_js_1
      .until(async resolve => {
        await client.last();
        const nseqno = Number(await client.runmethod(testgiverAddr, "seqno"));
        // console.warn("NEW",{nseqno,testgiverSeqNo})
        if (nseqno !== testgiverSeqNo) resolve();
        await this.sleep();
      })
      .then(async () => {
        // console.warn("DONE NUMBER")
        await this.client
          .waitInfoChange(this.saddr, "state", "empty")
          .catch(e => console.error(e));
        await this.client.sendFile(this.base + "-query.boc");
        await this.client
          .waitInfoChange(this.saddr, "state", "uninit")
          .catch(e => console.error(e));
        return this.info();
      })
      .catch(e => {
        console.error("probably a timeout?", e);
        throw e;
      });
  }
  async sendExternalLocal(keyPair, addr, amount, file) {
    const seqno = await this.client.runmethod(this.baddr, "seqno");
    const base = this.base + "-send";
    const fn = `${base}.boc`;
    const fn_hash = `${this.base}.send-hash`;
    await core_js_1.exec("rm -f", fn);
    await core_js_1.exec("rm -f", fn);
    if (file) {
      //seqno '0x'+ not used anymore
      throw "send with file not implemented";
      // let result = await this.client.fift.run(Account.SCRIPT_SEND, this.base, addr, seqno, amount, base, '-B', file)
      // console.error(result)
    } else {
      let result;
      result = await this.client.fift.run(
        Account.SCRIPT_WALLET_GEN_HASH,
        this.base,
        addr,
        seqno,
        amount,
        base
      );
      const hash = await core_js_1.fsp.readFile(fn_hash);
      await core_js_1.fsp.writeFile(
        this.base + ".send-sig",
        nacl.sign.detached(hash, keyPair.secretKey)
      );
      result = await this.client.fift.run(
        Account.SCRIPT_WALLET_GEN_BOC,
        this.base,
        addr,
        seqno,
        amount,
        base
      );
      console.log({ result });
    }
    let sendresult = await this.client.sendFile(fn);
    // console.warn("SEND RESULT ",sendresult)
    await core_js_1.until(async resolve => {
      await this.client.last();
      const nseqno = await this.client.runmethod(this.baddr, "seqno");
      // console.warn("NEW",{nseqno, seqno})
      if (nseqno !== seqno) resolve();
      await this.sleep();
    });
  }
  async sendExternal(keyPair, addrBuffer, addr, amount, file) {
    const seqno = Number(await this.client.runmethod(this.baddr, "seqno"));
    const uniqueSuffixB =
      new Date().getTime() + "-" + Math.round(Math.random() * 1e9);
    const fileBasename =
      core_js_1.TMPDIR +
      "/" +
      uuid_1.v5("sendext" + uniqueSuffixB, uuid_1.v5.URL);
    const uniqueSuffix =
      new Date().getTime() + "-" + Math.round(Math.random() * 1e9);
    const sendSigFileBasename = uuid_1.v5(
      "sendSigBuffer" + uniqueSuffix,
      uuid_1.v5.URL
    );
    const addrFile = {
      buffer: Buffer.from(addrBuffer),
      filename: sendSigFileBasename + ".addr"
    };
    if (file) {
      //seqno '0x'+ not used anymore
      throw "send with file not implemented";
      // let result = await this.client.fift.run(Account.SCRIPT_SEND, fileBasename, addr, seqno, amount, fileBasename, '-B', file)
      // console.error(result)
    } else {
      try {
        let hashResult = await this.client.fift.run(
          { type: "files", files: [addrFile] },
          Account.SCRIPT_WALLET_GEN_HASH,
          { type: "filepath", value: sendSigFileBasename },
          addr,
          seqno,
          amount,
          { type: "filepath", value: sendSigFileBasename }
        );
        console.log({ hashResult });
        if (!hashResult.sendHashBuffer) {
          throw new Error("send hash buffer not found!");
        }
        const hash = new Uint8Array(hashResult.sendHashBuffer.data);
        const sendSigBuffer = nacl.sign.detached(hash, keyPair.secretKey);
        console.log({ sendSigBuffer, hash });
        // console.warn("signature buffer generated", {sendSigFileBasename})
        const sendSig = {
          buffer: sendSigBuffer,
          filename: sendSigFileBasename + ".send-sig"
        };
        const fiftArgs = [
          { type: "files", files: [sendSig, addrFile] },
          Account.SCRIPT_WALLET_GEN_BOC,
          { type: "filepath", value: sendSigFileBasename },
          addr,
          seqno,
          amount,
          fileBasename
        ];
        let result = await this.client.fift.run(...fiftArgs);
        // console.warn("RESULT",result)
        // console.log(...fiftArgs)
        let sendresult = await this.client.sendFile(
          Buffer.from(result.bocBuffer)
        );
        return core_js_1
          .until(async resolve => {
            const nseqno = Number(
              await this.client.runmethod(this.baddr, "seqno")
            );
            // console.warn("NEW",{nseqno, seqno})
            if (nseqno !== seqno) {
              resolve();
            }
            await this.sleep(5000);
          })
          .catch(e => {
            console.error(e);
            throw e;
          });
      } catch (e) {
        console.error(e);
        throw e;
      }
    }
  }
  async send(addr, amount, file) {
    const seqno = Number(await this.client.runmethod(this.baddr, "seqno"));
    // console.error("OLD",{seqno})
    const base = this.base + "-send";
    const fn = `${base}.boc`;
    // console.log('sending', addr, amount)
    await core_js_1.exec("rm -f", fn);
    if (file) {
      //seqno '0x'+ not used anymore
      let result = await this.client.fift.run(
        Account.SCRIPT_SEND,
        this.base,
        addr,
        seqno,
        amount,
        base,
        "-B",
        file
      );
      // console.error(result)
    } else {
      let result = await this.client.fift.run(
        Account.SCRIPT_SEND,
        this.base,
        addr,
        seqno,
        amount,
        base
      );
      // console.log({result})
    }
    let sendresult = await this.client.sendFile(fn);
    console.warn("SEND RESULT ", sendresult);
    await core_js_1.until(async resolve => {
      await this.client.last();
      const nseqno = Number(await this.client.runmethod(this.baddr, "seqno"));
      // console.warn("NEW",{nseqno, seqno})
      if (nseqno !== seqno) {
        resolve();
      }
      await this.sleep();
    });
  }
}
exports.Account = Account;
Account.SCRIPT_NEW = core_js_1.join(core_js_1.PATH_FIFTLIB, "new-wallet.fif");
Account.SCRIPT_NEW_EXT = core_js_1.join(
  core_js_1.PATH_FIFT,
  "new-wallet-ext.fif"
);
Account.SCRIPT_SEND = core_js_1.join(core_js_1.PATH_FIFTLIB, "wallet.fif");
Account.SCRIPT_WALLET_GEN_HASH = core_js_1.join(
  core_js_1.PATH_FIFT,
  "wallet-gen-hash.fif"
);
Account.SCRIPT_WALLET_GEN_BOC = core_js_1.join(
  core_js_1.PATH_FIFT,
  "wallet-gen-boc.fif"
);
Account.SCRIPT_SHOW_ADDR = core_js_1.join(
  core_js_1.PATH_FIFTLIB,
  "show-addr.fif"
);
//# sourceMappingURL=account.js.map
