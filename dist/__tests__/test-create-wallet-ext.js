"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const tonkit_js_1 = require("../tonkit.js");
const nacl = tslib_1.__importStar(require("tweetnacl"));
const fs = tslib_1.__importStar(require("fs"));
const fsp = fs.promises;
process.on('unhandledRejection', error => {
    console.error('unhandledRejection');
    console.error(error);
    process.exit(2);
});
const PATH_DATA = tonkit_js_1.join(tonkit_js_1.PATH_ROOT, 'data');
const BASE = tonkit_js_1.join(PATH_DATA, 'test');
const FN_PK = BASE + '.pk';
const FN_SIG = BASE + '.sig';
const FN_HASH = tonkit_js_1.join(tonkit_js_1.PATH_FIXT, 'init-wallet.hash');
const SCRIPT_NEW_WALLET_EXT = tonkit_js_1.join(tonkit_js_1.PATH_FIFT, 'new-wallet-ext.fif');
const WORKCHAIN = -1;
const client = new tonkit_js_1.ClientLocal;
let result;
run();
async function run() {
    const { sign } = nacl;
    const kp = sign.keyPair();
    const hash = await fsp.readFile(FN_HASH);
    const sig = nacl.sign.detached(hash, kp.secretKey);
    await tonkit_js_1.exec('rm -rf ' + PATH_DATA);
    await tonkit_js_1.exec('mkdir ' + PATH_DATA);
    await fsp.writeFile(FN_PK, kp.publicKey);
    await fsp.writeFile(FN_SIG, sig);
    result = await client.fift.run(SCRIPT_NEW_WALLET_EXT, WORKCHAIN, BASE);
    console.log(result);
}
//# sourceMappingURL=test-create-wallet-ext.js.map