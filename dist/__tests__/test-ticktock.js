"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const tonkit_js_1 = require("../tonkit.js");
const fs = tslib_1.__importStar(require("fs"));
const fsp = fs.promises;
process.on('unhandledRejection', error => {
    console.error('unhandledRejection');
    console.error(error);
    process.exit();
});
const PATH_DATA = 'data';
const BASE = tonkit_js_1.join(PATH_DATA, 'test-ticktock');
const PATH_QUERY = tonkit_js_1.join(BASE + '-query.boc');
const SCRIPT_TEST_TICKTOCK = 'fift/new-ticktock.fif';
const client = new tonkit_js_1.ClientLocal;
run();
async function run() {
    let kp, hash, result, info, sig;
    await tonkit_js_1.exec('rm -rf ' + PATH_DATA);
    await tonkit_js_1.exec('mkdir ' + PATH_DATA);
    const account = await tonkit_js_1.Account.create('-1', BASE, client, SCRIPT_TEST_TICKTOCK);
    console.log({ saddr: account.saddr });
    info = await account.info();
    console.log({ info });
    if (info.state != 'active')
        await account.deploy();
    setInterval(async () => {
        info = await account.info();
        console.log({ info });
        await client.last();
        result = await client.runmethod(account.nbaddr, 'test');
        console.log({ result });
    }, 3000);
    /**/
}
//# sourceMappingURL=test-ticktock.js.map