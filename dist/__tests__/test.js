"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tonkit_js_1 = require("../tonkit.js");
const parse_1 = require("../parse");
let client = new tonkit_js_1.ClientLocal;
process.on('unhandledRejection', error => {
    console.error('unhandledRejection');
    console.error(error);
    process.exit(2);
});
const PATH_DATA = 'data';
const PATH_ACCOUNTS = tonkit_js_1.join(PATH_DATA, 'accounts');
async function init() {
    await tonkit_js_1.exec('rm', '-rf', tonkit_js_1.TMPDIR);
    await tonkit_js_1.exec('mkdir', "-p", tonkit_js_1.TMPDIR);
}
async function test() {
    await init();
    await healthcheck();
    await main();
}
let testgiverAddr = "kf9mZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZnw8";
testgiverAddr = "-1:6666666666666666666666666666666666666666666666666666666666666666";
let timer;
let result;
let testgiverSeqNo;
test();
async function healthcheck() {
    console.error("creating client");
    console.warn("creating test accounts");
    const testAccount1 = await tonkit_js_1.Account.create("-1", tonkit_js_1.TMPDIR + "/healthcheck-wallet-1", client);
    const testAccount0 = await tonkit_js_1.Account.create("0", tonkit_js_1.TMPDIR + "/healthcheck-wallet0", client);
    console.warn(testAccount1);
    console.warn("TESTING: fift create testgiver");
    const fiftResult = await client.fift.run(tonkit_js_1.PATH_FIFTLIB + "/new-testgiver.fif", -1, tonkit_js_1.TMPDIR + "/new-testgiver");
    // console.warn(fiftResult)  
}
function destroy() {
    client.destroy();
    client = new tonkit_js_1.ClientLocal;
    clearTimeout(timer);
}
async function timerFunc(msg, timeout = 20000) {
    destroy();
    console.warn("-");
    // console.info("--------------------------------------------------------")
    console.info(msg);
    timer = setTimeout(() => { console.error("test TIMEOUT", msg); client.destroy(); throw new Error(msg); }, timeout);
    console.warn(" ");
}
async function createwallet(walletName) {
    timerFunc("TESTING: runmethod ORIGINAL testgiver seqno" + walletName);
    await client.last();
    result = await client.runmethod(testgiverAddr, 'seqno');
    testgiverSeqNo = Number(result);
    console.warn("TESTGIVER SEQNO", { testgiverSeqNo });
    // console.warn("runmethod RESULT",{result: result, testgiverSeqNo:testgiverSeqNo})  
    timerFunc("TONKIT CREATE " + walletName);
    const testAccount1 = await tonkit_js_1.Account.create("-1", tonkit_js_1.TMPDIR + "/" + walletName, client);
    console.warn(testAccount1);
    timerFunc("FIFT testgiver-zerostate " + walletName);
    const fiftResultB = await client.fift.run(tonkit_js_1.PATH_FIFT + "/testgiver-zerostate.fif", testAccount1.nbaddr, testgiverSeqNo, 20, tonkit_js_1.TMPDIR + "/testgiver-zerostate");
    // console.log(fiftResultB)
    timerFunc("!!!!SENDFILE TESTGIVER " + walletName);
    result = await client.sendFile(tonkit_js_1.TMPDIR + '/testgiver-zerostate.boc');
    console.log("SEND FILE TESTGIVER RESULT", result);
    timerFunc("CREATE WALLET STATE CHANGE: BALANCE TO " + walletName);
    await client.waitInfoChange(testAccount1.nbaddr, 'state', 'empty').catch((e) => { console.error(e); });
    timerFunc("!!!!!!!SENDFILE DEPLOY " + walletName);
    result = await client.sendFile(tonkit_js_1.TMPDIR + '/' + walletName + '-query.boc');
    console.warn("sendFile NEW RESULT", { sendFile: result });
    timerFunc("TEST SHOW-ADDR SCRIPT " + walletName);
    result = await client.fift.run(tonkit_js_1.PATH_FIFTLIB + "/show-addr.fif", tonkit_js_1.TMPDIR + "/" + walletName);
    // console.warn({result})
    timerFunc("TESTING: info" + walletName);
    result = await testAccount1.info();
    console.warn({ result });
    timerFunc("DEPLOY: WAITING FOR uninit status" + walletName);
    result = await client.waitInfoChange(testAccount1.baddr, 'state', 'uninit').catch((e) => { console.error(e); });
    console.warn({ result });
    timerFunc("VERIFY: ACCOUNT seqno" + walletName);
    result = await client.runmethod(testAccount1.baddr, "seqno");
    // console.warn("getAccount RESULT",{result})
    timerFunc("WAIT FOR SEQUENCE NUMBER");
    await tonkit_js_1.until(async (resolve) => {
        const last = await client.last();
        // console.warn("LAST",last)
        const nseqno = Number(await client.runmethod(testgiverAddr, "seqno"));
        // console.warn("LAST SEQ NO",{nseqno,testgiverSeqNo})
        if (nseqno !== testgiverSeqNo)
            resolve();
    });
    return testAccount1;
}
async function testsend(amount, wallet1, wallet2) {
    result = await wallet2.info();
    let oldBalance = result.balance;
    timerFunc("TESTING: TONKIT send");
    const send = await wallet1.send(wallet2.baddr, amount);
    // console.warn(send)
    timerFunc("TESTSEND STATE CHANGE: BALANCE TO SELF");
    result = await wallet2.info();
    await client.waitInfoChange(wallet2.baddr, 'balance', oldBalance).catch((e) => { console.error(e); });
    timerFunc("TESTING: RESULT OF RECEIVING WALLET");
    result = await wallet2.info();
    console.warn("getAccount RESULT", { account: result });
    clearTimeout(timer);
    // timerFunc("TESTING: TONKIT send WITH MESSAGE")
    // result = await wallet1.send(wallet2.baddr, 0.00001, "testBoc")
    // console.warn(result)
}
async function new_testgiver() {
    timerFunc("FIFT NEW TESTGIVER");
    const newTestGiver = await client.fift.run(tonkit_js_1.PATH_FIFTLIB + "/new-testgiver.fif", -1, tonkit_js_1.TMPDIR + "/new-testgiver");
    console.warn(newTestGiver);
    let newTestGiverAddress = parse_1.nbaddr("newTestGiver");
    console.warn("ADDRESS OF NEW TESTGIVER", newTestGiverAddress);
    timerFunc("generate message to ZEROSTATE testgiver");
    const testgiverZerostate = await client.fift.run(tonkit_js_1.PATH_FIFT + "/testgiver-zerostate.fif", newTestGiverAddress, testgiverSeqNo, 20, tonkit_js_1.TMPDIR + "/testgiver-zerostate");
    // console.log(testgiverZerostate)
    timerFunc("!!!!SENDFILE ZEROSTATE TESTGIVER!!!");
    result = await client.sendFile(tonkit_js_1.TMPDIR + '/testgiver-zerostate.boc');
    console.log("SEND FILE TESTGIVER RESULT", result);
    timerFunc("TESTGIVER STATE CHANGE: BALANCE TO NEW TEST GIVER");
    await client.waitInfoChange(newTestGiverAddress, 'state', 'empty').catch((e) => { console.error(e); });
    timerFunc("!!!!!!!SENDFILE new-testgiver-query DEPLOY");
    result = await client.sendFile(tonkit_js_1.TMPDIR + '/new-testgiver-query.boc');
    console.warn("sendFile NEW RESULT", { sendFile: result });
    timerFunc("TESTING: getAccount NEW testgiver");
    result = await client.getaccount(newTestGiverAddress);
    // console.warn("getAccount RESULT",{account: result})
    timerFunc("TESTING: runmethod NEW testgiver seqno");
    result = await client.runmethod(newTestGiverAddress, 'seqno');
    testgiverSeqNo = Number(result);
    console.warn("NEW TESTGIVER SEQNO", testgiverSeqNo);
    console.warn("runmethod RESULT", { result: result, testgiverSeqNo: testgiverSeqNo });
    // This does not seem to be WORKING!!!!!!!
    timerFunc("WAITING FOR uninit status on NEW testgiver");
    await client.waitInfoChange(newTestGiverAddress, 'state', 'uninit').catch((e) => { console.error(e); });
    timerFunc("TESTING: NEW TESTGIVER ACCOUNT seqno");
    result = await client.runmethod(newTestGiverAddress, "seqno").catch((e) => console.error(e));
    // console.warn("getAccount RESULT",{account: result})  
    return this;
}
async function main() {
    timerFunc("LAST");
    result = await client.last();
    // console.warn("LAST RESULT",{last: result})  
    timerFunc("TESTING: getAccount test giver ZEROSTATE");
    result = await client.getaccount(testgiverAddr);
    console.warn("getAccount RESULT", { account: result });
    // new_testgiver()
    const wallet1 = await createwallet("/test-wallet1");
    const wallet2 = await createwallet("/test-wallet2");
    await testsend(1, wallet1, wallet2);
    console.error("FINISHED TEST");
    // console.warn("TESTING: query getstate last block")
    // result = await client.query('getstate '+lastBlockId, (ctx:string, resolve:any)=>{
    //   if(ctx.match(/external message status is 1/)) resolve()
    //   else throw new Error("getstate last block failed")
    // })
    // console.warn("QUERY RESULT",{query: result})  
    destroy();
    process.exit();
}
//# sourceMappingURL=test.js.map