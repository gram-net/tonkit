"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const tonkit_js_1 = require("../tonkit.js");
const nacl = tslib_1.__importStar(require("tweetnacl"));
const fs = tslib_1.__importStar(require("fs"));
const fsp = fs.promises;
process.on('unhandledRejection', error => {
    console.error('unhandledRejection');
    console.error(error);
    process.exit(2);
});
const PATH_DATA = 'data';
const BASE = tonkit_js_1.join(PATH_DATA, 'test');
const FN_SECKEY = BASE + '.sk';
const FN_HASH = BASE + '.hash';
const SCRIPT_TEST_SIGNING = 'fift/test-signing.fif';
const client = new tonkit_js_1.ClientRemote;
run();
async function run() {
    let kp, hash, result, sig;
    await tonkit_js_1.exec('rm -rf ' + PATH_DATA);
    await tonkit_js_1.exec('mkdir ' + PATH_DATA);
    result = await client.fift.run(SCRIPT_TEST_SIGNING, 0, BASE);
    console.log(result);
    result = await fsp.readFile(FN_SECKEY);
    console.log(result);
    kp = nacl.sign.keyPair.fromSeed(result);
    console.log(kp);
    hash = await fsp.readFile(FN_HASH);
    console.log(hash);
    sig = nacl.sign.detached(hash, kp.secretKey);
    console.log(sig);
}
//# sourceMappingURL=test-sign.js.map