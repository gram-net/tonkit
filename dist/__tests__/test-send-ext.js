"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const tonkit_js_1 = require("../tonkit.js");
const nacl = tslib_1.__importStar(require("tweetnacl"));
const fs = tslib_1.__importStar(require("fs"));
const fsp = fs.promises;
process.on('unhandledRejection', error => {
    console.error('unhandledRejection');
    console.error(error);
    process.exit(2);
});
const PATH_DATA = 'data';
const BASE = tonkit_js_1.join(PATH_DATA, 'test');
const BASE_W1 = tonkit_js_1.join(PATH_DATA, 'w1');
const BASE_W2 = tonkit_js_1.join(PATH_DATA, 'w2');
const FN_PK = BASE + '.pk';
const FN_SIG = BASE + '.sig';
const FN_HASH = tonkit_js_1.join(tonkit_js_1.PATH_FIXT, 'fixt/init-wallet.hash');
const SCRIPT_NEW_WALLET_EXT = tonkit_js_1.join(tonkit_js_1.PATH_FIFT, 'fift/new-wallet-ext.fif');
const WORKCHAIN = 0;
const client = new tonkit_js_1.ClientLocal;
const BASE_MAIN_WALLET = '/var/ton-work/zerostate/main-wallet';
let result;
run();
async function reset() {
    await tonkit_js_1.exec('rm -rf ' + PATH_DATA);
    await tonkit_js_1.exec('mkdir ' + PATH_DATA);
}
async function test() {
    console.log('creating keyPair');
    const keyPair1 = nacl.sign.keyPair();
    const keyPair = nacl.sign.keyPair();
    console.log('creating w1');
    const w1 = await tonkit_js_1.Account.create('-1', BASE_W1, client);
    console.log('creating w2');
    const w2 = await tonkit_js_1.Account.createExternal('-1', BASE_W2, keyPair, client);
    let info;
    console.log('deploying');
    await w1.deploy();
    await check();
    console.log('sending from w1 to w2');
    await client.last();
    await w1.send(w2.nbaddr, 5);
    await check();
    await client.waitInfoChange(w2.saddr, 'state', 'empty');
    await check();
    await w2.sendInitQuery();
    await check();
    console.log('sending from w2 to w1');
    await w2.sendExternal(keyPair, fs.readFileSync(BASE_W2 + ".addr"), w1.nbaddr, 2.5);
    await check();
    async function check() {
        info = await w1.info();
        console.log({ w1, info });
        info = await w2.info();
        console.log({ w2, info });
    }
}
async function run() {
    await reset();
    await test();
}
//# sourceMappingURL=test-send-ext.js.map