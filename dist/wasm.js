"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Wasm {
  constructor(wasmSrc, assetPath = ".") {
    this.log = [];
    this.err = [];
    this.wasmSrc = wasmSrc;
    this.assetPath = assetPath;
    return this;
  }
  async exec() {
    return new Promise((resolve, reject) => {
      let wasmObj = this.wasmSrc(
        {
          onRuntimeInitialized: async () => {
            await this.onRuntimeInitialized(wasmObj);
            resolve(this);
          },
          onAbort: e => reject(e),
          print: x => this.log.push(x),
          printErr: x => this.err.push(x)
        },
        this.assetPath,
        []
      ).then(() => {});
    });
  }
}
exports.Wasm = Wasm;
class Func extends Wasm {
  constructor(wasmSrc, files, flags = [], assetPath) {
    super(wasmSrc, assetPath);
    this.onRuntimeInitialized = this.init;
    this.files = files;
    this.flags = flags;
    return this;
  }
  async init(wasmObj) {
    for (let i in this.files) {
      this.flags.push("/" + this.files[i].name);
      wasmObj.FS.writeFile("/" + this.files[i].name, this.files[i].data);
    }
    wasmObj.callMain(this.flags);
  }
}
exports.Func = Func;
//# sourceMappingURL=wasm.js.map
