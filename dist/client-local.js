"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const core_js_1 = require("./core.js");
const fs = tslib_1.__importStar(require("fs"));
const CMD_LC = "lite-client";
const DEBUG_CLIENT = 0;
const DEBUG_CLIENT_CMD = DEBUG_CLIENT & 0;
const DEBUG_CLIENT_CTX = DEBUG_CLIENT & 0;
const DEBUG_CLIENT_QRY = DEBUG_CLIENT & 0;
const TON_IP = process.env.TON_IP || "localhost";
const LITESERVER_PORT = process.env.LITESERVER_PORT || "6304";
const LS_PUB = process.env.LS_PUB || "/tmp/tonkit/liteserver.pub";
class ClientLocal {
  constructor(args, autoLast, wasmFift, wasmFunc) {
    // it's a noop, see client-remote.ts
    this.blobFromBuffer = buffer => buffer;
    let ARGS = ["-a", `${TON_IP}:${LITESERVER_PORT}`, "-p", `${LS_PUB}`];
    this.args = args || ARGS;
    // console.log(this.args)
    this.idleWorkers = [];
    this.autoLast = autoLast;
    if (wasmFunc && wasmFift) {
      this.fift = new core_js_1.fift(wasmFift);
    } else {
      this.fift = new core_js_1.fift();
    }
  }
  timerFunc(msg, timeout = 20000) {
    // console.warn("TIMERFUNC")
    // console.info(msg)
    const self = this;
    this.timer = setTimeout(async () => {
      console.error("ClientLocal TIMEOUT", msg, timeout);
      await self.destroy();
      throw new Error(msg);
    }, timeout);
  }
  last() {
    return this.query("last", (ctx, resolve, reject) => {
      // console.warn({ctx})
      if (ctx.match(/last masterchain block/gs)) resolve(ctx);
    });
  }
  async sendFile(file) {
    let fn = file;
    if (typeof file === "object") {
      const uniqueSuffixB =
        new Date().getTime() + "-" + Math.round(Math.random() * 1e9);
      fn = core_js_1.TMPDIR + "/" + uniqueSuffixB;
      fs.writeFileSync(fn, file);
    }
    return this.query("sendfile " + fn, (ctx, resolve, reject) => {
      if (ctx.match(/external message status is 1/)) {
        resolve();
      } else if (ctx.match(/error/)) {
        reject(ctx);
      }
    });
  }
  async sleep(ms = 2500) {
    return new Promise(resolve => {
      setTimeout(resolve, ms);
    });
  }
  async waitInfoChange(account, param, oldValue) {
    // console.warn('waitInfoChange', {param, oldValue})
    return await core_js_1.until(async (resolve, reject) => {
      let last = await this.last();
      // console.warn(account,last)
      const info = await this.getaccount(account);
      // console.warn('waitInfoChange', {info})
      if (info[param] !== oldValue) resolve();
      await this.sleep();
    });
  }
  static getAccountHandle(ctx, resolve, reject) {
    const info = { state: "unknown", balance: 0 };
    let match = ctx.match(/account state is \(/);
    // console.warn("--------------------------------------------------------_",ctx)
    if (match) {
      match = ctx.match(/state:\(?account_(active|uninit).*\n/);
      if (match) {
        info.state = match[1];
      } else {
        reject("could not parse state:\n" + ctx);
      }
      match = ctx.match(
        /grams:\(nanograms\s*amount:\(var_uint len:[0-9]* value:([0-9]*)/
      );
      if (match) {
        info.balance = Number(match[1]);
      } else {
        reject("could not parse balance\n" + ctx);
      }
      resolve(info);
    } else {
      match = ctx.match(/account state is empty/);
      if (match) {
        info.state = "empty";
        resolve(info);
      } else {
        match = ctx.match(/\[(Error : [^\]]*)\]/);
        if (match) {
          reject(match[0]);
        }
      }
    }
  }
  async getaccount(address) {
    await this.last();
    const result = await this.query(
      `getaccount ${address}`,
      ClientLocal.getAccountHandle
    );
    return result;
  }
  static runMethodHandle(ctx, resolve, reject) {
    // console.error("---------------------------___________________________",ctx)
    let match = ctx.match(/not initialized yet/);
    if (match) {
      return reject(`runmethod error not initialized`);
    }
    match = ctx.match(/result:\s*\[([^\]]*)/);
    if (match) {
      resolve(match[1].trim());
    } else {
      match = ctx.match(/result:\serror\s([0-9]*)/);
      if (match) {
        reject(`runmethod error ${match[1]}`);
      } else {
        match = ctx.match(/\[(Error : -?[0-9]* : [^\]]*)\]/);
        if (match) {
          reject(`runmethod error ${match[1]}`);
        } else {
          match = ctx.match(/account state of -?[0-9]*:[0-9A-F]* is empty/);
          if (match) {
            reject(`runmethod error ${match[0]}`);
          }
        }
      }
    }
  }
  async runmethod(address, method) {
    const last = await this.last();
    // console.warn(last)
    const q = `runmethod ${address} ${method}`;
    // console.warn(q)
    const result = await this.query(q, ClientLocal.runMethodHandle);
    // console.warn(result)
    return result;
  }
  // internal only?
  async query(q, handle) {
    const { idleWorkers, autoLast, args, timer } = this;
    const self = this;
    if (autoLast && q !== "last") await this.last();
    if (DEBUG_CLIENT_QRY) console.log("DEBUG_CLIENT_QUERY", q);
    let worker = idleWorkers.pop();
    // if(worker) console.info("IDLE WORKER USED. !!!!!!00----------------------IDLE WORKERS LEFT:",idleWorkers.length)
    if (!worker) {
      worker = await createWorker(args); //idleWorkers.pop() ||
      // console.info("NEW WORKER HAS BEEN CREATED")
    }
    const { stdin, stderr, stdout } = worker; //, stdout NO
    // this.timerFunc(q)
    return new Promise((resolve, reject) => {
      const iresolve = arg => {
        stderr.off("data", ondata);
        stdout.off("data", ondata);
        // stdout.off('data', ondata) NO
        idleWorkers.push(worker);
        // clearTimeout(timer)
        resolve(arg);
      };
      stderr.on("data", ondata);
      stdout.on("data", ondata);
      // stdout.on('data', ondata) NO
      stdin.write(q + "\n");
      function ondata(data) {
        const ctx = data.toString();
        if (DEBUG_CLIENT_CTX)
          console.info("______________CTX___________", { ctx });
        handle(ctx, iresolve, reject);
      }
    });
  }
  async destroy() {
    let worker;
    // console.warn("DEAD WORKERS", this.idleWorkers.length)
    while ((worker = this.idleWorkers.pop())) {
      worker.kill();
    }
    // clearTimeout(this.timer)
  }
}
exports.ClientLocal = ClientLocal;
async function createWorker(args) {
  return new Promise((resolve, reject) => {
    const onexit = () => {
      reject("failed to initialize");
    };
    if (DEBUG_CLIENT_CMD) console.log(CMD_LC, ...args);
    const worker = core_js_1.cp_spawn(CMD_LC, args);
    const { stderr, stdout } = worker;
    worker.on("exit", onexit);
    stderr.on("data", ondata);
    stdout.on("data", ondata);
    function ondata(data) {
      const ctx = data.toString();
      if (DEBUG_CLIENT_CTX)
        console.log("-----------worker output--------------", { ctx });
      if (ctx.match(/obtained [0-9]* data bytes for block/)) {
        worker.off("exit", onexit);
        stderr.off("data", ondata);
        stdout.off("data", ondata);
        resolve(worker);
      }
    }
  });
}
//# sourceMappingURL=client-local.js.map
