declare type Client = any;
export interface AddressObj {
    saddr: string;
    nbaddr: string;
    baddr: string;
    wc: string | number;
    addrHex?: string;
}
export declare class Account implements AddressObj {
    saddr: string;
    nbaddr: string;
    baddr: string;
    wc: string;
    constructor(base: string, client: Client, saddr: string, nbaddr: string, baddr: string);
    get base(): any;
    get client(): any;
    static SCRIPT_NEW: string;
    static SCRIPT_NEW_EXT: string;
    static SCRIPT_SEND: string;
    static SCRIPT_WALLET_GEN_HASH: string;
    static SCRIPT_WALLET_GEN_BOC: string;
    static SCRIPT_SHOW_ADDR: string;
    static create(wc: any, base: string, client: Client, script?: string): Promise<Account>;
    static createExternal(wc: string, base: string, keyPair: any, client: Client): Promise<Account>;
    sleep(ms?: number): Promise<unknown>;
    static load(base: string, client: Client): Promise<Account>;
    static fromAddressObj(base: string, addressObj: AddressObj, client: Client): Account;
    static fromFiftRes(fiftRes: string, base: string, client: Client): Account;
    sendInitQuery(): Promise<void>;
    info(): Promise<any>;
    txs(): Promise<import("axios").AxiosResponse<import("./tonkit.js").Transaction[]>>;
    deploy(deployFile?: any): Promise<any>;
    sendExternalLocal(keyPair: any, addr: string, amount: number, file?: string): Promise<void>;
    sendExternal(keyPair: any, addrBuffer: any, addr: string, amount: number, file?: string): Promise<unknown>;
    send(addr: string, amount: number, file?: string): Promise<void>;
}
export {};
