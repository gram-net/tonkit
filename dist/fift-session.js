"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const core_js_1 = require("./core.js");
const client_local_js_1 = require("./client-local.js");
const wasm_js_1 = require("./wasm.js");
const ADDR_GIFTER = "kf9mZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZnw8";
const cpMap = new WeakMap();
/*
  Fift interactive session
*/
class FiftSession {
  constructor() {
    cpMap.set(
      this,
      child_process_1.spawn("fift", "-L TonUtil.fif -L Asm.fif".split(" "))
    );
  }
  async func(name, files, flags = []) {
    const func = require("../wasm/func.js");
    let funcObj = new wasm_js_1.Func(func, files, flags);
    await funcObj.exec();
    if (funcObj.err.length || !funcObj.log.length)
      return new Error(funcObj.err.join("\n"));
    const result = funcObj.log.join("\n");
    return this.write(`${result} =: ${name}`);
  }
  async cell(name, ops) {
    return this.write(`<b ${ops} b> =: ${name}`);
  }
  async hash(name) {
    return this.hex(name, "hashB");
  }
  async boc(name) {
    return this.hex(name, "boc>B");
  }
  async hex(name, ops) {
    return core_js_1.fromHex(
      (await this.write(`${name} ${ops || ""} .dump`)).split(":")[1]
    );
  }
  async set(expr, name) {
    return this.write(`${expr} =: ${name}`);
  }
  async addr(wc, state_init) {
    // normal, nbaddr, baddr
    const result = (
      await this.write(`
      ${state_init} dup hashu ${wc} swap
      2dup .addr cr
      2dup 7 .Addr cr
      6 .Addr cr`)
    ).split("\n");
    return { saddr: result[0].trim(), nbaddr: result[1], baddr: result[2] };
  }
  async gift(addr, amt, client) {
    client = client || new client_local_js_1.ClientLocal();
    const seqno = await client.runmethod(ADDR_GIFTER, "seqno");
    return this.hex(`
      Masterchain 0x6666666666666666666666666666666666666666666666666666666666666666 2constant giver_addr
      "${addr}" true parse-load-address drop 2=: dest_addr
      ${seqno} =: seqno
      "${amt}" $>GR =: amount
      // create a message (NB: 01b00.., b = bounce)
      <b b{01} s, 0 1 i, b{000100} s, dest_addr addr, 
        amount Gram, 0 9 64 32 + + 1+ 1+ u, 0 32 u, "GIFT" $, b>
      <b seqno 32 u, 1 8 u, swap ref, b>
      <b b{1000100} s, giver_addr addr, 0 Gram, b{00} s,
        swap <s s, b>
      2 boc+>B
    `);
  }
  async write(input) {
    const lines = input.split("\n");
    const cp = cpMap.get(this);
    const { stdin, stdout, stderr } = cp;
    const output = [];
    let i = 0;
    for (let line of lines) await writeLine(line);
    function writeLine(line) {
      return new Promise((res, rej) => {
        ++i;
        stdout.on("data", onout);
        stderr.on("data", onerr);
        stdin.write(line + "\n", null);
        function onout(d) {
          const str = d.toString();
          if (str.endsWith(" ok\n")) {
            output.push(str.substr(0, str.length - 4));
            unhook();
            res();
          } else {
            output.push(str);
          }
        }
        function onerr(d) {
          unhook();
          rej(`error on line(${i}): ${line}\n` + d);
        }
        function unhook() {
          stdout.off("data", onout);
          stderr.off("data", onerr);
        }
      });
    }
    return output.join("");
  }
}
exports.FiftSession = FiftSession;
//# sourceMappingURL=fift-session.js.map
