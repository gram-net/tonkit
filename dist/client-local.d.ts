/// <reference types="node" />
import { ChildProcessWithoutNullStreams } from 'child_process';
export declare class ClientLocal {
    args: string[];
    autoLast: boolean;
    fift: any;
    func: any;
    timer: any;
    idleWorkers: ChildProcessWithoutNullStreams[];
    constructor(args?: string[], autoLast?: boolean, wasmFift?: any, wasmFunc?: any);
    timerFunc(msg: any, timeout?: number): void;
    last(): Promise<any>;
    blobFromBuffer: (buffer: Uint8Array) => Uint8Array;
    sendFile(file: any): Promise<any>;
    sleep(ms?: number): Promise<unknown>;
    waitInfoChange(account: string, param: string, oldValue: any): Promise<unknown>;
    static getAccountHandle(ctx: string, resolve: any, reject: any): void;
    getaccount(address: string): Promise<any>;
    static runMethodHandle(ctx: string, resolve: any, reject: any): any;
    runmethod(address: string, method: string): Promise<any>;
    query(q: string, handle: any): Promise<any>;
    destroy(): Promise<void>;
}
