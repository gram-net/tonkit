export declare type filesExp = Array<{
    name: string;
    data: ArrayBuffer | string;
}>;
export declare class Wasm {
    log: Array<string>;
    err: Array<string>;
    assetPath: string;
    wasmSrc: any;
    onRuntimeInitialized: Function;
    constructor(wasmSrc: any, assetPath?: string);
    exec(): Promise<unknown>;
}
export declare class Func extends Wasm {
    files: filesExp;
    flags: Array<string>;
    constructor(wasmSrc: any, files: filesExp, flags?: Array<string>, assetPath?: string);
    init(wasmObj: any): Promise<void>;
}
