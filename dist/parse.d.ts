export declare function saddr(ctx: string): string;
export declare function nbaddr(ctx: string): string;
export declare function baddr(ctx: string): string;
export declare function state(ctx: string): string;
export declare function grams(ctx: string): string;
export declare function seqno(ctx: string): string;
export declare function hash(ctx: string): string;
