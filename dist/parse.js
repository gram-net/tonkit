"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function saddr(ctx) {
  return getMatch(ctx.match(/=\s(-?[0-9]*:[0-9a-zA-Z\-_]*)/));
}
exports.saddr = saddr;
function nbaddr(ctx) {
  return getMatch(ctx.match(/:\s(0[0-9a-zA-Z\-_]{47})/));
}
exports.nbaddr = nbaddr;
function baddr(ctx) {
  return getMatch(ctx.match(/:\s(k[0-9a-zA-Z\-_]{47})/));
}
exports.baddr = baddr;
function state(ctx) {
  return getMatch(ctx.match(/state:\(?account_(active|uninit).*\n/));
}
exports.state = state;
function grams(ctx) {
  return getMatch(
    ctx.match(/grams:\(nanograms\s*amount:\(var_uint len:[0-9]* value:([0-9]*)/)
  );
}
exports.grams = grams;
function seqno(ctx) {
  return getMatch(ctx.match(/x{([0-9A-F]{8})[0-9A-F_]*}\s*last transaction/));
}
exports.seqno = seqno;
function hash(ctx) {
  return getMatch(ctx.match(/hash = ([0-9A-F]*)/));
}
exports.hash = hash;
function getMatch(m) {
  if (m) {
    return m[1].toString();
  } else {
    return "";
  }
}
//# sourceMappingURL=parse.js.map
