"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const axios_1 = tslib_1.__importDefault(require("axios"));
const core_js_1 = require("./core.js");
const tweetnacl_1 = require("tweetnacl");
const uuid_1 = require("uuid");
const form_data_1 = tslib_1.__importDefault(require("form-data"));
const transaction_js_1 = require("./models/transaction.js");
class ClientRemote {
  constructor(autoLast) {
    this.remote = true;
    this.blobFromBuffer = buffer => new Blob([new Uint8Array(buffer).buffer]);
    // API
    this.tfnuri = process.env.TFN_API_URI || "http://localhost";
    this.tfnport = process.env.TFN_API_PORT || 8084;
    this.ApiBase = `${this.tfnuri}:${this.tfnport}`;
    this.fift = {
      run: async (...args) => {
        const endpoint = `${this.ApiBase}/fift`;
        // const config: AxiosRequestConfig = {
        //   headers: {
        //     "Content-Type": "application/json"
        //   }
        // };
        let form = new form_data_1.default();
        // console.warn(args)
        for (let i = 0; i < args.length; i++) {
          if (typeof args[i] == "object" && args[i].type == "filepath") {
            form.append(i.toString(), "filepath:" + args[i].value);
          } else if (typeof args[i] == "object" && args[i].type == "files") {
            let fileBuffersObj = args[i].files;
            for (var x = 0; x < fileBuffersObj.length; x++) {
              const fileBlob = this.blobFromBuffer(fileBuffersObj[x].buffer);
              form.append(fileBuffersObj[x].filename, fileBlob, {
                filename: fileBuffersObj[x].filename
              });
            }
          } else {
            // console.warn(typeof args[i], args[i])
            form.append(i.toString(), args[i]);
          }
        }
        const headers =
          typeof form.getHeaders === "function" ? form.getHeaders() : {};
        const config = { headers };
        return axios_1.default
          .post(endpoint, form, config)
          .then(response => {
            this.destroy();
            return response.data;
          })
          .catch(e => {
            console.error(e);
            this.destroy();
            throw e;
          });
      }
    };
    this.func = {
      run: async (...args) => {
        const endpoint = `${this.ApiBase}/func`;
        let form = new form_data_1.default();
        // console.warn(args)
        for (let i = 0; i < args.length; i++) {
          if (typeof args[i] == "object" && args[i].type == "filepath") {
            form.append(i.toString(), "filepath:" + args[i].value);
          } else if (typeof args[i] == "object" && args[i].type == "files") {
            let fileBuffersObj = args[i].files;
            for (var x = 0; x < fileBuffersObj.length; x++) {
              const fileBlob = this.blobFromBuffer(fileBuffersObj[x].buffer);
              form.append(fileBuffersObj[x].filename, fileBlob, {
                filename: fileBuffersObj[x].filename
              });
            }
          } else {
            // console.warn(typeof args[i], args[i])
            form.append(i.toString(), args[i]);
          }
        }
        const headers =
          typeof form.getHeaders === "function" ? form.getHeaders() : {};
        const config = { headers };
        return axios_1.default
          .post(endpoint, form, config)
          .then(response => {
            this.destroy();
            return response.data;
          })
          .catch(e => {
            this.destroy();
            throw e;
          });
      }
    };
    this.autoLast = autoLast;
  }
  timerFunc(msg, timeout = 20000) {
    // console.info(msg)
    const self = this;
    this.timer = setTimeout(async () => {
      console.error("ClientRemote TIMEOUT", msg, timeout);
      throw new Error(msg);
    }, timeout);
  }
  async destroy() {
    clearTimeout(this.timer);
  }
  async fetchJson(url, data) {
    const config = {
      method: "get",
      url,
      data,
      headers: {
        "Content-Type": "application/json"
      }
    };
    // console.warn(endpoint, JSON.stringify(data), config)
    return axios_1.default(config);
  }
  async last() {
    const endpoint = `${this.ApiBase}/last`;
    const data = await this.fetchJson(endpoint);
    return data;
  }
  /**
   *
   * @param baddr wallet address format `baddr`
   */
  async txs(baddr) {
    const endpoint = `${this.ApiBase}/transactions?account=${baddr}`;
    const res = await this.fetchJson(endpoint);
    if (res.data && res.status === 200) {
      res.data = transaction_js_1.mapTransactions(res.data);
    }
    return res;
  }
  async sleep(ms = 2500) {
    return new Promise(resolve => {
      setTimeout(resolve, ms);
    });
  }
  async waitInfoChange(account, param, oldValue) {
    console.warn("waitInfoChange", { param, oldValue });
    return await core_js_1.until(async (resolve, reject) => {
      // let last = await this.last()
      // console.warn("LAST?", last)
      const info = await this.getaccount(account);
      // console.warn('waitInfoChange', { info })
      if (info[param] !== oldValue) {
        console.warn("waitInfoChange complete", { info, param, oldValue });
        resolve();
      }
      await this.sleep();
    });
  }
  async getaccount(address) {
    const endpoint = `${this.ApiBase}/getaccount?address=${address}`;
    const response = await this.fetchJson(endpoint);
    // console.warn("GOT ACCOUNT DATA", response.data)
    return response.data;
  }
  async block(block) {
    const endpoint = `${this.ApiBase}/block?block=${block}`;
    const res = await this.fetchJson(endpoint);
    return res;
  }
  async forgeaccount(opts) {
    let {
      sig,
      workchain,
      privateKey,
      publicKey,
      hashBuffer,
      hashFift,
      createFiftPath
    } = opts;
    // console.warn(opts)
    if (hashBuffer) {
      sig = this.blobFromBuffer(
        tweetnacl_1.sign.detached(hashBuffer, privateKey)
      );
    } else if (hashFift) {
      throw new Error("not implemented hashFift");
    } else {
      throw new Error(
        "Nope... must pass either hashFile or hashFift file path"
      );
    }
    let form = new form_data_1.default();
    const pubkey = this.blobFromBuffer(publicKey);
    // console.log({ sig, pubkey })
    form.append(
      "createFiftPath",
      createFiftPath || core_js_1.PATH_FIFT + "/new-wallet-ext.fif"
    );
    //  form.append('createFiftFile', createFiftBuffer, { filename: uuidv5('createFiftFile', uuidv5.URL) })
    form.append("workchain", workchain);
    form.append("pubkeyFile", pubkey, {
      filename: uuid_1.v5("pubkeyFile", uuid_1.v5.URL)
    });
    form.append("sigFile", sig, {
      filename: uuid_1.v5("sigFile", uuid_1.v5.URL)
    });
    // console.warn("forgeaccount form",form)
    const headers =
      typeof form.getHeaders === "function" ? form.getHeaders() : {};
    const endpoint = `${this.ApiBase}/forgeaccount`;
    const config = { headers };
    return axios_1.default
      .post(endpoint, form, config)
      .then(response => {
        this.destroy();
        return response.data;
      })
      .catch(e => {
        console.error(e);
        this.destroy();
        throw e;
      });
  }
  async sendFile(file) {
    let form = new form_data_1.default();
    // console.warn(typeof file, file)
    if (typeof file === "string") {
      throw Error("Can't use FS");
      // form.append("bocFile", createReadStream(file));
    } else {
      form.append("bocFile", this.blobFromBuffer(file), {
        filename: uuid_1.v5("fileBuffer", uuid_1.v5.URL)
      });
    }
    // console.warn("sendFile form", form)
    const headers =
      typeof form.getHeaders === "function" ? form.getHeaders() : {};
    const endpoint = `${this.ApiBase}/sendFile`;
    const config = { headers };
    return axios_1.default
      .post(endpoint, form, config)
      .then(response => {
        this.destroy();
        return response.data;
      })
      .catch(e => {
        this.destroy();
        throw e;
      });
  }
  async runmethod(address, method) {
    const data = { address, method };
    const endpoint = `${this.ApiBase}/runmethod`;
    const config = {
      headers: {
        "Content-Type": "application/json"
      }
    };
    return axios_1.default
      .post(endpoint, data, config)
      .then(response => {
        this.destroy();
        return response.data;
      })
      .catch(e => {
        this.destroy();
        throw e;
      });
  }
}
exports.ClientRemote = ClientRemote;
//# sourceMappingURL=client-remote.js.map
