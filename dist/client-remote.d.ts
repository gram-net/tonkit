import { AxiosResponse } from "axios";
import { Transaction } from "./models/transaction.js";
export declare type AddressState = "frozen" | "uninitialized";
export interface ForgeaccountOptions {
    sig?: Blob;
    workchain: number;
    privateKey: Uint8Array;
    publicKey: Uint8Array;
    createFiftBuffer?: string;
    createFiftPath?: string;
    hashBuffer?: any;
    hashFile?: string;
    hashFift?: string;
}
export declare class ClientRemote {
    autoLast: boolean;
    timer: any;
    remote: boolean;
    blobFromBuffer: (buffer: Uint8Array) => Blob;
    constructor(autoLast?: boolean);
    tfnuri: string;
    tfnport: string | number;
    ApiBase: string;
    timerFunc(msg: any, timeout?: number): void;
    destroy(): Promise<void>;
    fetchJson<T = any>(url: string, data?: {
        [key: string]: any;
    }): Promise<AxiosResponse<T>>;
    last(): Promise<AxiosResponse<any>>;
    /**
     *
     * @param baddr wallet address format `baddr`
     */
    txs(baddr: string): Promise<AxiosResponse<Transaction[]>>;
    sleep(ms?: number): Promise<unknown>;
    waitInfoChange(account: string, param: string, oldValue: any): Promise<unknown>;
    getaccount(address: string): Promise<any>;
    block(block: string): Promise<AxiosResponse<any>>;
    fift: {
        run: (...args: any[]) => Promise<any>;
    };
    func: {
        run: (...args: any) => Promise<any>;
    };
    forgeaccount(opts: ForgeaccountOptions): Promise<any>;
    sendFile(file: any): Promise<any>;
    runmethod(address: string, method: string): Promise<any>;
}
