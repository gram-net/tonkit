import { ClientLocal } from './client-local.js';
import { ClientRemote } from './client-remote.js';
import { filesExp } from './wasm.js';
export declare class FiftSession {
    constructor();
    func(name: string, files: filesExp, flags?: string[]): Promise<string | Error>;
    cell(name: string, ops: string): Promise<string>;
    hash(name: string): Promise<Uint8Array>;
    boc(name: string): Promise<Uint8Array>;
    hex(name: string, ops?: string): Promise<Uint8Array>;
    set(expr: any, name: string): Promise<string>;
    addr(wc: string, state_init: string): Promise<{
        saddr: string;
        nbaddr: string;
        baddr: string;
    }>;
    gift(addr: string, amt: number, client?: ClientLocal | ClientRemote): Promise<Uint8Array>;
    write(input: string): Promise<string>;
}
