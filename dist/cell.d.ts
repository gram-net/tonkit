export declare class Builder {
    constructor();
    int(num: bigint, len: bigint): this;
    uint(num: bigint, len: bigint): this;
    ref(cell: Cell): this;
    end(): Cell;
}
export declare class Cell {
    constructor(builder: Builder);
    get hash(): ArrayBuffer;
}
export declare class Slice {
}
