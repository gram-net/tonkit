"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const path_1 = require("path");
exports.dirname = path_1.dirname;
exports.join = path_1.join;
exports.normalize = path_1.normalize;
const child_process_1 = require("child_process");
exports.cp_exec = child_process_1.exec;
exports.cp_spawn = child_process_1.spawn;
const fs = tslib_1.__importStar(require("fs"));
exports.fsp = fs.promises;
exports.DEBUG = 0;
exports.DEBUG_FIFT = 1 && exports.DEBUG;
exports.DEBUG_FIFT_COMMAND = 1 && exports.DEBUG_FIFT;
exports.DEBUG_FIFT_OUTPUT = 1 && exports.DEBUG_FIFT;
let fiftlib = process.env.FIFTLIB || "../tfn-ton/crypto/smartcont";
exports.PATH_FIFTLIB = path_1.join(fiftlib);
exports.PATH_ROOT = path_1.normalize(path_1.join(__dirname, ".."));
exports.PATH_FIFT = path_1.join(exports.PATH_ROOT, "fift");
exports.PATH_FIXT = path_1.join(exports.PATH_ROOT, "fixt");
exports.TMPDIR = path_1.join(exports.PATH_ROOT, "tmp");
exports.FIFTPATH = process.env.FIFTPATH || "../tfn-ton/crypto/fift/lib";
exports.log = console.log.bind(console);
function toHex(arg) {
  return Buffer.from(arg).toString("hex");
}
exports.toHex = toHex;
function fromHex(str) {
  return Uint8Array.from(Buffer.from(str, "hex"));
}
exports.fromHex = fromHex;
function exec(...args) {
  return new Promise((res, rej) => {
    const command = args.join(" ");
    if (exports.DEBUG_FIFT_COMMAND) console.log({ command });
    child_process_1.exec(command, (error, stdout, stderr) => {
      if (error) {
        error.message += { stdout, stderr }.toString();
        rej(error);
      } else {
        res(stdout.toString() + stderr.toString());
      }
    });
  });
}
exports.exec = exec;
function handleUnhandledRejections() {
  process.on("unhandledRejection", error => {
    console.error("unhandledRejection");
    console.error(error);
    process.exit();
  });
}
exports.handleUnhandledRejections = handleUnhandledRejections;
class fift {
  constructor(fift) {
    this.fift = fift;
    return this;
  }
  destroy() {
    clearTimeout(this.timer);
    this.running = false;
  }
  timerFunc(msg, timeout = 20000) {
    this.destroy();
    this.timer = setTimeout(() => {
      console.error("core fift TIMEOUT:" + msg);
      throw new Error(msg);
    }, timeout);
  }
  async run(...args) {
    if (this.running) {
      throw new Error("already running");
    }
    this.running = true;
    this.timerFunc(args.join(" "));
    const self = this;
    args.unshift("-s");
    args.unshift("-I" + exports.FIFTPATH);
    this.cmdArgs = [];
    for (let i = 0; i < args.length; i++) {
      if (typeof args[i] == "object" && args[i].type == "filepath") {
        this.cmdArgs.push(exports.TMPDIR + "/" + args[i].value);
      } else if (typeof args[i] == "object" && args[i].type == "files") {
        let fileBuffersObj = args[i].files;
        for (var x = 0; x < fileBuffersObj.length; x++) {
          exports.fsp.writeFile(
            exports.TMPDIR + "/" + fileBuffersObj[x].filename,
            fileBuffersObj[x].buffer
          );
        }
      } else {
        // console.warn(typeof args[i], args[i])
        this.cmdArgs.push(args[i]);
      }
    }
    // console.warn("USING COMMAND LINE EXECUTABLE FIFT", args, this.cmdArgs)
    return exec("fift", ...this.cmdArgs)
      .then(result => {
        // if(DEBUG_FIFT_OUTPUT) console.log("FIFT RESULTS",{result})
        self.res = result;
        self.destroy();
        let outBuffer, bocBuffer, sendHashBuffer;
        let filebasename = self.cmdArgs.pop();
        let outFile = filebasename;
        let bocFile = filebasename + ".boc";
        let sendHashFile = filebasename + ".send-hash";
        if (fs.existsSync(outFile)) {
          outBuffer = fs.readFileSync(outFile);
        }
        if (fs.existsSync(bocFile)) {
          bocBuffer = fs.readFileSync(bocFile);
        }
        if (fs.existsSync(sendHashFile)) {
          sendHashBuffer = fs.readFileSync(sendHashFile);
        }
        return { res: self.res, bocBuffer, sendHashBuffer, outBuffer };
      })
      .catch(error => {
        console.error(error);
        this.destroy();
        throw error;
      });
  }
}
exports.fift = fift;
function until(fn, delay = 2000, retries = 30) {
  const pr = new Promise((resolve, reject) => {
    let retriesDone = 0;
    const interval = setInterval(callfn, delay);
    const iresolve = () => {
      clearInterval(interval);
      resolve();
    };
    function callfn() {
      retriesDone++;
      if (retriesDone >= retries) {
        clearInterval(interval);
        return reject(new Error("TIMEOUT DURING UNTIL"));
      }
      fn(iresolve);
    }
  });
  return pr;
}
exports.until = until;
//# sourceMappingURL=core.js.map
