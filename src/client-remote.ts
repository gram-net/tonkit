import axios, { AxiosResponse, AxiosRequestConfig, AxiosPromise } from "axios";
import { until, PATH_FIFT } from "./core.js";

import { sign as nacl_sign } from "tweetnacl";
import { v5 as uuidv5 } from "uuid";

import FormData from "form-data";
import {
  mapTransactions,
  TransactionResponse,
  Transaction,
} from "./models/transaction.js";

export type AddressState = "frozen" | "uninitialized";

export interface ForgeaccountOptions {
  sig?: Blob;
  workchain: number;
  privateKey: Uint8Array;
  publicKey: Uint8Array;
  createFiftBuffer?: string;
  createFiftPath?: string;
  hashBuffer?: any;
  hashFile?: string;
  hashFift?: string;
}

export class ClientRemote {
  autoLast: boolean;
  timer: any;
  remote = true;

  blobFromBuffer = (buffer: Uint8Array) =>
    new Blob([new Uint8Array(buffer).buffer]);

  constructor(autoLast?: boolean) {
    this.autoLast = autoLast;
  }

  // API
  tfnuri = process.env.TFN_API_URI || "http://localhost";
  tfnport = process.env.TFN_API_PORT || 8084;
  ApiBase = `${this.tfnuri}:${this.tfnport}`;

  timerFunc(msg: any, timeout = 20000) {
    // console.info(msg)
    const self = this;
    this.timer = setTimeout(async () => {
      console.error("ClientRemote TIMEOUT", msg, timeout);
      throw new Error(msg);
    }, timeout);
  }

  async destroy() {
    clearTimeout(this.timer);
  }

  async fetchJson<T = any>(url: string, data?: { [key: string]: any }) {
    const config: AxiosRequestConfig = {
      method: "get",
      url,
      data,
      headers: {
        "Content-Type": "application/json",
      },
    };
    // console.warn(endpoint, JSON.stringify(data), config)
    return axios(config) as AxiosPromise<T>;
  }

  async last() {
    const endpoint = `${this.ApiBase}/last`;
    const data = await this.fetchJson(endpoint);
    return data;
  }

  /**
   *
   * @param baddr wallet address format `baddr`
   */
  async txs(baddr: string): Promise<AxiosResponse<Transaction[]>> {
    const endpoint = `${this.ApiBase}/transactions?account=${baddr}`;
    const res = await this.fetchJson<TransactionResponse[]>(endpoint);
    if (res.data && res.status === 200) {
      ((res.data as any) as Transaction[]) = mapTransactions(res.data);
    }
    return (res as any) as AxiosResponse<Transaction[]>;
  }

  async sleep(ms = 2500) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }

  async waitInfoChange(account: string, param: string, oldValue: any) {
    console.warn("waitInfoChange", { param, oldValue });
    return await until(async (resolve: any, reject: any) => {
      // let last = await this.last()
      // console.warn("LAST?", last)
      const info: any = await this.getaccount(account);
      // console.warn('waitInfoChange', { info })
      if (info[param] !== oldValue) {
        console.warn("waitInfoChange complete", { info, param, oldValue });
        resolve();
      }
      await this.sleep();
    });
  }

  async getaccount(address: string) {
    const endpoint = `${this.ApiBase}/getaccount?address=${address}`;
    const response = await this.fetchJson(endpoint);
    // console.warn("GOT ACCOUNT DATA", response.data)
    return response.data;
  }

  async block(block: string) {
    const endpoint = `${this.ApiBase}/block?block=${block}`;
    const res = await this.fetchJson(endpoint);
    return res;
  }

  fift = {
    run: async (...args: Array<any>) => {
      const endpoint = `${this.ApiBase}/fift`;
      // const config: AxiosRequestConfig = {
      //   headers: {
      //     "Content-Type": "application/json"
      //   }
      // };

      let form = new FormData();

      // console.warn(args)
      for (let i = 0; i < args.length; i++) {
        if (typeof args[i] == "object" && args[i].type == "filepath") {
          form.append(i.toString(), "filepath:" + args[i].value);
        } else if (typeof args[i] == "object" && args[i].type == "files") {
          let fileBuffersObj = args[i].files;
          for (var x = 0; x < fileBuffersObj.length; x++) {
            const fileBlob = this.blobFromBuffer(fileBuffersObj[x].buffer);
            form.append(fileBuffersObj[x].filename, fileBlob, {
              filename: fileBuffersObj[x].filename,
            });
          }
        } else {
          // console.warn(typeof args[i], args[i])
          form.append(i.toString(), args[i]);
        }
      }
      const headers =
        typeof form.getHeaders === "function" ? form.getHeaders() : {};
      const config: AxiosRequestConfig = { headers };

      return axios
        .post(endpoint, form, config)
        .then((response: any) => {
          this.destroy();
          return response.data;
        })
        .catch((e: any) => {
          console.error(e);
          this.destroy();
          throw e;
        });
    },
  };

  func = {
    run: async (...args: any) => {
      const endpoint = `${this.ApiBase}/func`;

      let form = new FormData();
      // console.warn(args)
      for (let i = 0; i < args.length; i++) {
        if (typeof args[i] == "object" && args[i].type == "filepath") {
          form.append(i.toString(), "filepath:" + args[i].value);
        } else if (typeof args[i] == "object" && args[i].type == "files") {
          let fileBuffersObj = args[i].files;
          for (var x = 0; x < fileBuffersObj.length; x++) {
            const fileBlob = this.blobFromBuffer(fileBuffersObj[x].buffer);
            form.append(fileBuffersObj[x].filename, fileBlob, {
              filename: fileBuffersObj[x].filename,
            });
          }
        } else {
          // console.warn(typeof args[i], args[i])
          form.append(i.toString(), args[i]);
        }
      }

      const headers =
        typeof form.getHeaders === "function" ? form.getHeaders() : {};
      const config: AxiosRequestConfig = { headers };

      return axios
        .post(endpoint, form, config)
        .then((response: any) => {
          this.destroy();
          return response.data;
        })
        .catch((e: any) => {
          this.destroy();
          throw e;
        });
    },
  };

  async forgeaccount(opts: ForgeaccountOptions) {
    let {
      sig,
      workchain,
      privateKey,
      publicKey,
      hashBuffer,
      hashFift,
      createFiftPath,
    } = opts;
    // console.warn(opts)

    if (hashBuffer) {
      sig = this.blobFromBuffer(nacl_sign.detached(hashBuffer, privateKey));
    } else if (hashFift) {
      throw new Error("not implemented hashFift");
    } else {
      throw new Error(
        "Nope... must pass either hashFile or hashFift file path"
      );
    }

    let form = new FormData();
    const pubkey = this.blobFromBuffer(publicKey);
    // console.log({ sig, pubkey })
    form.append(
      "createFiftPath",
      createFiftPath || PATH_FIFT + "/new-wallet-ext.fif"
    );
    //  form.append('createFiftFile', createFiftBuffer, { filename: uuidv5('createFiftFile', uuidv5.URL) })

    form.append("workchain", workchain);
    form.append("pubkeyFile", pubkey, {
      filename: uuidv5("pubkeyFile", uuidv5.URL),
    });
    form.append("sigFile", sig, { filename: uuidv5("sigFile", uuidv5.URL) });

    // console.warn("forgeaccount form",form)
    const headers =
      typeof form.getHeaders === "function" ? form.getHeaders() : {};
    const endpoint = `${this.ApiBase}/forgeaccount`;
    const config: AxiosRequestConfig = { headers };
    return axios
      .post(endpoint, form, config)
      .then((response: any) => {
        this.destroy();
        return response.data;
      })
      .catch((e: any) => {
        console.error(e);
        this.destroy();
        throw e;
      });
  }

  async sendFile(file: any) {
    let form = new FormData();
    // console.warn(typeof file, file)
    if (typeof file === "string") {
      throw Error("Can't use FS");
      // form.append("bocFile", createReadStream(file));
    } else {
      form.append("bocFile", this.blobFromBuffer(file), {
        filename: uuidv5("fileBuffer", uuidv5.URL),
      });
    }
    // console.warn("sendFile form", form)
    const headers =
      typeof form.getHeaders === "function" ? form.getHeaders() : {};

    const endpoint = `${this.ApiBase}/sendFile`;
    const config: AxiosRequestConfig = { headers };
    return axios
      .post(endpoint, form, config)
      .then((response: any) => {
        this.destroy();
        return response.data;
      })
      .catch((e: any) => {
        this.destroy();
        throw e;
      });
  }

  async runmethod(address: string, method: string) {
    const data = { address, method };
    const endpoint = `${this.ApiBase}/runmethod`;
    const config: AxiosRequestConfig = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    return axios
      .post(endpoint, data, config)
      .then((response: any) => {
        this.destroy();
        return response.data;
      })
      .catch((e: any) => {
        this.destroy();
        throw e;
      });
  }
}
