import { dirname, join, normalize } from 'path'
import { exec as cp_exec, spawn as cp_spawn } from 'child_process'
import * as fs from 'fs'

import { rejects } from 'assert'
import { errorMonitor } from 'events'

export { dirname, join, normalize }
export { cp_exec, cp_spawn }

export const fsp = fs.promises

export const DEBUG = 0
export const DEBUG_FIFT = 1 && DEBUG
export const DEBUG_FIFT_COMMAND = 1 && DEBUG_FIFT
export const DEBUG_FIFT_OUTPUT = 1 && DEBUG_FIFT
let fiftlib = process.env.FIFTLIB || "../tfn-ton/crypto/smartcont"
export const PATH_FIFTLIB = join(fiftlib)
export const PATH_ROOT = normalize(join(__dirname, '..'))
export const PATH_FIFT = join(PATH_ROOT, 'fift')
export const PATH_FIXT = join(PATH_ROOT, 'fixt')
export const TMPDIR = join(PATH_ROOT, 'tmp')
export const FIFTPATH = process.env.FIFTPATH || '../tfn-ton/crypto/fift/lib'

export const log = console.log.bind(console)

export function toHex(arg:any) {
  return Buffer.from(arg).toString('hex')
}

export function fromHex(str:string) {
  return Uint8Array.from(Buffer.from(str, 'hex'))
}

export function exec(...args: (string | Buffer)[]): Promise<string> {
  return new Promise((res, rej) => {
    const command = args.join(' ')
    if (DEBUG_FIFT_COMMAND) console.log({command})
    cp_exec(command, (error, stdout, stderr) => {
      if(error) {
        error.message += {stdout, stderr}.toString()
        rej(error)
      } else {
        res(stdout.toString() + stderr.toString())
      }
    })
  })
}

export function handleUnhandledRejections() {
  process.on('unhandledRejection', error => {
    console.error('unhandledRejection')
    console.error(error)
    process.exit()
  })
}

export class fift {
  fift: any
  res: any
  timer: any
  running: boolean
  cmdArgs: Array<any>
  constructor(fift?: any) {
    this.fift = fift
    return this
  }
  destroy() {
    clearTimeout(this.timer)
    this.running = false
  }
  timerFunc(msg: any, timeout = 20000) {
    this.destroy()
    this.timer = setTimeout(() => { console.error("core fift TIMEOUT:" + msg); throw new Error(msg) }, timeout)
  }
  async run(...args: any) {
    if (this.running) {
      throw new Error("already running")
    }
    this.running = true
    this.timerFunc(args.join(' '))
    const self = this
    args.unshift("-s")
    args.unshift("-I" + FIFTPATH)

    this.cmdArgs = []

    for (let i = 0; i < args.length; i++) {
      if (typeof args[i] == 'object' && args[i].type == "filepath") {
        this.cmdArgs.push(TMPDIR + "/" + args[i].value)
      } else if (typeof args[i] == 'object' && args[i].type == "files") {
        let fileBuffersObj = args[i].files
        for (var x = 0; x < fileBuffersObj.length; x++) {
          fsp.writeFile(TMPDIR + "/" + fileBuffersObj[x].filename, fileBuffersObj[x].buffer)
        }
      } else {
        // console.warn(typeof args[i], args[i])
        this.cmdArgs.push(args[i])
      }
    }
    // console.warn("USING COMMAND LINE EXECUTABLE FIFT", args, this.cmdArgs)
    return exec('fift', ...this.cmdArgs).then((result) => {
      // if(DEBUG_FIFT_OUTPUT) console.log("FIFT RESULTS",{result})
      self.res = result
      self.destroy()

      let outBuffer, bocBuffer, sendHashBuffer
      let filebasename = self.cmdArgs.pop()
      let outFile = filebasename
      let bocFile = filebasename + ".boc"
      let sendHashFile = filebasename + ".send-hash"
      if (fs.existsSync(outFile)) {
        outBuffer = fs.readFileSync(outFile)
      }
      if (fs.existsSync(bocFile)) {
        bocBuffer = fs.readFileSync(bocFile)
      }
      if (fs.existsSync(sendHashFile)) {
        sendHashBuffer = fs.readFileSync(sendHashFile)
      }

      return { res: self.res, bocBuffer, sendHashBuffer, outBuffer }
    }).catch((error) => {
      console.error(error)
      this.destroy()
      throw error
    })
  }
}

export function until(fn: any, delay: number = 2000, retries: number = 30) {
  const pr = new Promise((resolve, reject) => {
    let retriesDone = 0
    const interval = setInterval(callfn, delay)
    const iresolve = () => {
      clearInterval(interval)
      resolve()
    }
    function callfn() {
      retriesDone++
      if (retriesDone >= retries) {
        clearInterval(interval)
        return reject(new Error("TIMEOUT DURING UNTIL"))
      }
      fn(iresolve)
    }
  })
  return pr
}