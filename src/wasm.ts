export type filesExp = Array<{ name: string, data: ArrayBuffer | string }>

export class Wasm {
  log: Array<string>;
  err: Array<string>;
  assetPath: string;
  wasmSrc: any;
  onRuntimeInitialized: Function;
  constructor(wasmSrc:any, assetPath:string=".") {
    this.log = [];
    this.err = [];
    this.wasmSrc = wasmSrc;
    this.assetPath = assetPath; 
    return this
  }
  async exec() {
    return new Promise((resolve, reject) => {
      let wasmObj = this.wasmSrc({
        onRuntimeInitialized: async () => {
          await this.onRuntimeInitialized(wasmObj)
          resolve(this)
        },
        onAbort: (e: any) => reject(e),
        print: (x: any) => this.log.push(x),
        printErr: (x: any) => this.err.push(x),
      }, this.assetPath, []).then(() => {

      })
    })
  }
}

export class Func extends Wasm { 
  files: filesExp;
  flags: Array<string>
  constructor(wasmSrc:any, files:filesExp, flags:Array<string>=[], assetPath?:string) {
    super(wasmSrc, assetPath)
    this.onRuntimeInitialized = this.init
    this.files = files;
    this.flags = flags;  
    return this
  }
  async init(wasmObj: any) {
    for(let i in this.files) {     
      this.flags.push("/"+this.files[i].name)
      wasmObj.FS.writeFile("/"+this.files[i].name, this.files[i].data);
    }
    
    wasmObj.callMain(this.flags)      
  }
}