import { ClientRemote } from './client-remote.js';
// import { ClientRemote as Client } from './client-remote'
import { saddr, nbaddr, baddr } from './parse.js'
import { PATH_FIFT, PATH_FIXT, PATH_FIFTLIB, TMPDIR, until, exec, join, fsp } from './core.js'
import * as nacl from 'tweetnacl'
import { v5 as uuidv5 } from 'uuid'

type Client = any

const bases = new Map
const clients = new Map
const FN_HASH = join(PATH_FIXT, 'init-wallet.hash')

export interface AddressObj {
  saddr: string;
  nbaddr: string;
  baddr: string;
  wc: string | number;
  addrHex?: string;
}

export class Account implements AddressObj {
  wc: string;

  constructor(
    base: string,
    client: Client,
    public saddr: string,
    public nbaddr: string,
    public baddr: string,
  ) {
    bases.set(this, base)
    clients.set(this, client)
    this.wc = saddr.split(':')[0]
  }

  get base() { return bases.get(this) }
  get client() { return clients.get(this) }

  static SCRIPT_NEW = join(PATH_FIFTLIB, 'new-wallet.fif')
  static SCRIPT_NEW_EXT = join(PATH_FIFT, 'new-wallet-ext.fif')
  static SCRIPT_SEND = join(PATH_FIFTLIB, 'wallet.fif')
  static SCRIPT_WALLET_GEN_HASH = join(PATH_FIFT, 'wallet-gen-hash.fif')
  static SCRIPT_WALLET_GEN_BOC = join(PATH_FIFT, 'wallet-gen-boc.fif')
  static SCRIPT_SHOW_ADDR = join(PATH_FIFTLIB, 'show-addr.fif')

  static async create(wc: any, base: string, client: Client, script: string = Account.SCRIPT_NEW) {
    await client.fift.run(script, wc.toString(), base)
    return Account.load(base, client)
  }

  static async createExternal(wc: string, base: string, keyPair: any, client: Client) {
    const hash = await fsp.readFile(FN_HASH)
    await fsp.writeFile(base + '.pk', keyPair.publicKey)
    await fsp.writeFile(base + '.sig', nacl.sign.detached(hash, keyPair.secretKey))
    await client.fift.run(Account.SCRIPT_NEW_EXT, wc.toString(), base)
    return Account.load(base, client)
  }

  async sleep(ms = 2500) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }

  static async load(base: string, client: Client) {
    const fiftResult: any = await client.fift.run(Account.SCRIPT_SHOW_ADDR, base)
    // console.warn(base, client, fiftResult)
    // console.warn(saddr(fiftResult.res), nbaddr(fiftResult.res), baddr(fiftResult.res))
    return new Account(base, client, saddr(fiftResult.res), nbaddr(fiftResult.res), baddr(fiftResult.res))
  }

  static fromAddressObj(base: string, addressObj: AddressObj, client: Client) {
    return new Account(base, client, addressObj.saddr, addressObj.nbaddr, addressObj.baddr)
  }

  static fromFiftRes(fiftRes: string, base: string, client: Client) {
    return new Account(base, client, saddr(fiftRes), nbaddr(fiftRes), baddr(fiftRes))
  }

  async sendInitQuery() {
    await this.client.sendFile(this.base + '-query.boc')
    await this.client.waitInfoChange(this.saddr, 'state', 'uninit').catch((e: any) => console.error(e))
  }

  async info() {
    const result = await this.client.getaccount(this.baddr)
    return result
  }

  async txs() {
    const result = await (this.client as ClientRemote).txs(this.baddr)
    return result
  }

  async deploy(deployFile?: any) {
    const result1 = await this.client.last()

    let testgiverAddr = "kf9mZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZnw8"

    let result = await this.client.runmethod(testgiverAddr, 'seqno')
    let testgiverSeqNo = Number(result)
    // console.warn("TESTGIVER SEQNO", {testgiverSeqNo})

    const bocPath = TMPDIR + "/testgiver-zerostate-" + uuidv5('testgiver-zerostate-' + testgiverSeqNo, uuidv5.URL)

    const fiftResultB: any = await this.client.fift.run(join(PATH_FIFT, 'testgiver-zerostate.fif'), this.nbaddr, testgiverSeqNo, 20, bocPath)
    // console.log(fiftResultB)
    if (this.client.remote) {
      result = await this.client.sendFile(Buffer.from(fiftResultB.bocBuffer))

    } else {
      result = await this.client.sendFile(bocPath + ".boc")

    }
    // console.log("SEND FILE TESTGIVER RESULT",result)
    const { client } = this
    // console.warn("WAIT FOR SEQUENCE NUMBER")
    return until(async (resolve: any) => {
      await client.last()

      const nseqno = Number(await client.runmethod(testgiverAddr, "seqno"))
      // console.warn("NEW",{nseqno,testgiverSeqNo})
      if (nseqno !== testgiverSeqNo) resolve()
      await this.sleep()
    }).then(async () => {
      // console.warn("DONE NUMBER")
      await this.client.waitInfoChange(this.saddr, 'state', 'empty').catch((e: any) => console.error(e))
      await this.client.sendFile(this.base + '-query.boc')
      await this.client.waitInfoChange(this.saddr, 'state', 'uninit').catch((e: any) => console.error(e))
      return this.info()
    }).catch((e: any) => {
      console.error("probably a timeout?", e)
      throw e
    })
  }

  async sendExternalLocal(keyPair: any, addr: string, amount: number, file?: string) {
    const seqno = await this.client.runmethod(this.baddr, "seqno")
    const base = this.base + '-send'
    const fn = `${base}.boc`
    const fn_hash = `${this.base}.send-hash`
    await exec('rm -f', fn)
    await exec('rm -f', fn)
    if (file) { //seqno '0x'+ not used anymore
      throw 'send with file not implemented'
      // let result = await this.client.fift.run(Account.SCRIPT_SEND, this.base, addr, seqno, amount, base, '-B', file)
      // console.error(result)
    } else {
      let result
      result = await this.client.fift.run(Account.SCRIPT_WALLET_GEN_HASH, this.base, addr, seqno, amount, base)
      const hash = await fsp.readFile(fn_hash)
      await fsp.writeFile(this.base + '.send-sig', nacl.sign.detached(hash, keyPair.secretKey))
      result = await this.client.fift.run(Account.SCRIPT_WALLET_GEN_BOC, this.base, addr, seqno, amount, base)
      console.log({ result })
    }
    let sendresult = await this.client.sendFile(fn)
    // console.warn("SEND RESULT ",sendresult)
    await until(async (resolve: any) => {
      await this.client.last()
      const nseqno = await this.client.runmethod(this.baddr, "seqno")
      // console.warn("NEW",{nseqno, seqno})
      if (nseqno !== seqno) resolve()
      await this.sleep()
    })
  }

  async sendExternal(keyPair: any, addrBuffer: any, addr: string, amount: number, file?: string) {
    const seqno = Number(await this.client.runmethod(this.baddr, "seqno"))
    const uniqueSuffixB = new Date().getTime() + '-' + Math.round(Math.random() * 1E9)

    const fileBasename = TMPDIR + "/" + uuidv5('sendext' + uniqueSuffixB, uuidv5.URL)
    const uniqueSuffix = new Date().getTime() + '-' + Math.round(Math.random() * 1E9)

    const sendSigFileBasename = uuidv5("sendSigBuffer" + uniqueSuffix, uuidv5.URL)
    const addrFile = { buffer: Buffer.from(addrBuffer), filename: sendSigFileBasename + '.addr' }

    if (file) { //seqno '0x'+ not used anymore
      throw 'send with file not implemented'
      // let result = await this.client.fift.run(Account.SCRIPT_SEND, fileBasename, addr, seqno, amount, fileBasename, '-B', file)
      // console.error(result)
    } else {
      try {
        let hashResult = await this.client.fift.run({ type: "files", files: [addrFile] }, Account.SCRIPT_WALLET_GEN_HASH, { type: "filepath", value: sendSigFileBasename }, addr, seqno, amount, { type: "filepath", value: sendSigFileBasename })
        console.log({ hashResult })
        if (!hashResult.sendHashBuffer) {
          throw new Error('send hash buffer not found!')
        }
        const hash = new Uint8Array(hashResult.sendHashBuffer.data)
        const sendSigBuffer = nacl.sign.detached(hash, keyPair.secretKey);
        console.log({ sendSigBuffer, hash })
        // console.warn("signature buffer generated", {sendSigFileBasename})
        const sendSig = { buffer: sendSigBuffer, filename: sendSigFileBasename + '.send-sig' }
        const fiftArgs = [{ type: "files", files: [sendSig, addrFile] }, Account.SCRIPT_WALLET_GEN_BOC, { type: "filepath", value: sendSigFileBasename }, addr, seqno, amount, fileBasename]
        let result = await this.client.fift.run(...fiftArgs)
        // console.warn("RESULT",result)

        // console.log(...fiftArgs)
        let sendresult = await this.client.sendFile(Buffer.from(result.bocBuffer))
        return until(async (resolve: any) => {
          const nseqno = Number(await this.client.runmethod(this.baddr, "seqno"))
          // console.warn("NEW",{nseqno, seqno})
          if (nseqno !== seqno) {
            resolve()
          }
          await this.sleep(5000)
        }).catch((e: any) => {
          console.error(e)
          throw e
        })
      } catch (e) {
        console.error(e)
        throw e
      }
    }

  }

  async send(addr: string, amount: number, file?: string) {
    const seqno = Number(await this.client.runmethod(this.baddr, "seqno"))
    // console.error("OLD",{seqno})
    const base = this.base + '-send'
    const fn = `${base}.boc`
    // console.log('sending', addr, amount)
    await exec('rm -f', fn)
    if (file) { //seqno '0x'+ not used anymore
      let result = await this.client.fift.run(Account.SCRIPT_SEND, this.base, addr, seqno, amount, base, '-B', file)
      // console.error(result)
    } else {
      let result = await this.client.fift.run(Account.SCRIPT_SEND, this.base, addr, seqno, amount, base)
      // console.log({result})
    }
    let sendresult = await this.client.sendFile(fn)
    console.warn("SEND RESULT ", sendresult)
    await until(async (resolve: any) => {
      await this.client.last()
      const nseqno = Number(await this.client.runmethod(this.baddr, "seqno"))
      // console.warn("NEW",{nseqno, seqno})
      if (nseqno !== seqno) {
        resolve()
      }
      await this.sleep()
    })
  }

}