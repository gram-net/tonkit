const MAX_REFS = 4

const map:WeakMap<any,CellData> = new WeakMap

class CellData {
  
  rep:bigint
  len:bigint
  ref:Cell[]
  
  constructor(rep?:bigint, len?:bigint, ...ref:Cell[]) {
    this.rep = rep || 0n
    this.len = len || 0n
    this.ref = ref
  }

}

export class Builder {
  
  constructor() {
    map.set(this, new CellData)
  }

  int(num:bigint, len:bigint) {
    if(num < 0) {

    } else {
      return this.uint(num, len)
    }
  }

  uint(num:bigint, len:bigint) {
    const data = map.get(this)
    const mask = (2n ** (len + 1n)) - 1n
    data.rep = (data.rep << len) + (mask & num)
    data.len += len
    return this
  }

  ref(cell:Cell) {
    const data = map.get(this)
    if(data.ref.length == MAX_REFS) throw new Error('max refs')
    data.ref.push(cell)
    return this
  }

  end():Cell {
    return new Cell(this)
  }

}

export class Cell {
  
  constructor(builder:Builder) {

  }

  get hash():ArrayBuffer {
    const buffer = new ArrayBuffer(32)
    
    return buffer
  }

}

export class Slice {

}