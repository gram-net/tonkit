import {
  ClientLocal as Client,
  join,
  exec,
  PATH_FIFT,
  PATH_FIXT,
  PATH_ROOT
} from '../tonkit.js'

import * as nacl from 'tweetnacl'
import * as fs from 'fs'

const fsp = fs.promises

process.on('unhandledRejection', error => {
  console.error('unhandledRejection')
  console.error(error)
  process.exit(2)
})

const PATH_DATA = join(PATH_ROOT,'data')
const BASE = join(PATH_DATA, 'test')
const FN_PK = BASE + '.pk'
const FN_SIG = BASE + '.sig'
const FN_HASH = join(PATH_FIXT, 'init-wallet.hash')
const SCRIPT_NEW_WALLET_EXT = join(PATH_FIFT, 'new-wallet-ext.fif')
const WORKCHAIN = -1
const client = new Client

let result

run()

async function run() {

  const {sign} = nacl
  const kp = sign.keyPair()
  const hash = await fsp.readFile(FN_HASH)
  const sig = nacl.sign.detached(hash, kp.secretKey)
  
  await exec('rm -rf ' + PATH_DATA)
  await exec('mkdir ' + PATH_DATA)
  await fsp.writeFile(FN_PK, kp.publicKey)
  await fsp.writeFile(FN_SIG, sig)

  result = await client.fift.run(SCRIPT_NEW_WALLET_EXT, WORKCHAIN, BASE)
  console.log(result)

}