import {
  ClientLocal as Client,
  Account,
  join,
  exec
} from '../tonkit.js'

import * as nacl from 'tweetnacl'
import * as fs from 'fs'

const fsp = fs.promises

process.on('unhandledRejection', error => {
  console.error('unhandledRejection')
  console.error(error)
  process.exit()
})

const PATH_DATA = 'data'
const BASE = join(PATH_DATA, 'test-ticktock')
const PATH_QUERY = join(BASE+'-query.boc')
const SCRIPT_TEST_TICKTOCK = 'fift/new-ticktock.fif'

const client = new Client

run()

async function run() {

  let kp, hash, result, info, sig

  await exec('rm -rf ' + PATH_DATA)
  await exec('mkdir ' + PATH_DATA)
  
  
  const account = await Account.create('-1', BASE, client, SCRIPT_TEST_TICKTOCK)
  console.log({saddr: account.saddr})
  info = await account.info()
  console.log({info})
  if(info.state != 'active') await account.deploy()
  
  setInterval(async ()=>{
    info = await account.info()
    console.log({info})
    await client.last()
    result = await client.runmethod(account.nbaddr, 'test')
    console.log({result})
  }, 3000)

  /**/

}