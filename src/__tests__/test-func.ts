import {
  ClientLocal as Client,
  join,
  handleUnhandledRejections,
  PATH_FIXT,
  FiftSession, toHex,
} from '../tonkit.js'

const PATH_STDLIB = join(process.env.TON_SMARTCONT, 'stdlib.fc')

import nacl from 'tweetnacl'
import {readFileSync} from 'fs'

handleUnhandledRejections()
run()

async function run() {
  
  const SRC = `
;; Simple wallet smart contract

() recv_internal(slice in_msg) impure {
  ;; do nothing for internal messages
}

() recv_external(slice in_msg) impure {
  var signature = in_msg~load_bits(512);
  var cs = in_msg;
  int msg_seqno = cs~load_uint(32);
  var cs2 = begin_parse(get_data());
  var stored_seqno = cs2~load_uint(32);
  var public_key = cs2~load_uint(256);
  cs2.end_parse();
  throw_unless(33, msg_seqno == stored_seqno);
  throw_unless(34, check_signature(slice_hash(in_msg), signature, public_key));
  accept_message();
  ;; cs~touch();
  if (cs.slice_refs()) {
    var mode = cs~load_uint(8);
    send_raw_message(cs~load_ref(), mode);
  }
  cs.end_parse();
  set_data(begin_cell().store_uint(stored_seqno + 1, 32).store_uint(public_key, 256).end_cell());
}

;; make error
` // remove ;; on last line to produce func parsing error

  const client = new Client
  const log = console.log.bind(console)
  const FC_TEST = join(PATH_FIXT, 'test.fc')
  const {publicKey, secretKey} = nacl.sign.keyPair()
  const sess = new FiftSession

  let info, sig_array, sig, addr, boc_gift, boc_deploy, wc = 0
  
  await sess.set(0, 'wc') // set workchain
  const stdlibfc = readFileSync(PATH_STDLIB).toString()
  let files = [
    {name:'stdlib',data:stdlibfc},
    {name:'simple-wallet',data:SRC}
  ]
  await sess.func('code', files, ["-P"])
  // await sess.funcFile('code', FC_TEST) // comment above line and uncomment this to switch to using a .fc file
  
  await sess.cell('data', `0 32 u, x{${toHex(publicKey)}} s,`)
  await sess.cell('body', '0 32 u,')
  await sess.cell('state_init', 'b{0011} s, code ref, data ref, null dict,')
  
  addr = await sess.addr('wc', 'state_init')

  log({addr})
  sig_array = nacl.sign.detached(await sess.hash('body'), secretKey)
  sig = toHex(sig_array)
  await sess.cell('msg', 
    `b{10} s, ` + // ext_in_msg_info$10
    `b{00} s, ` + // src:MsgAddressInt (addr_none$00)
    `b{10} s, b{0} s, ` + // dst:MsgAddressInt [addr_std$10, no$0{anycast:(Maybe Anycast)}]
    `${wc} state_init hashu addr, ` + // wc state_init_hash===account_address
    `b{0000} s, ` + // import_fee:Grams = 0
    `b{10} s, ` + // init:(Maybe (Either StateInit ^StateInit)) [1=yes-has init, 0=not a ^ref] (state init is part of current slice as opposed to ref to other cell)
      `state_init <s s, ` + // converts state_init cell to a slice and appends it to this cell's bits
    `b{0} s, ` + // body is not a ref
      `x{${sig}} s, ` + // signature
      `body <s s,` // body
  )
 
  boc_deploy = await sess.boc('msg')
  boc_gift = await sess.gift(addr.nbaddr, 5, client)
  await client.sendFile(boc_gift)
  log('waiting to receive gram gift...')
  await client.waitInfoChange(addr.saddr, 'state', 'empty')
  log('account received gram gift')
  await client.sendFile(boc_deploy)
  log('waiting for initialization...')
  await client.waitInfoChange(addr.saddr, 'state', 'uninit')
  log('account is active')
  await client.destroy()
  info = await client.getaccount(addr.saddr)
  log({info})
  log('done') 
}