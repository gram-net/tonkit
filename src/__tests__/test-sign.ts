import {
  ClientRemote as Client,
  join,
  exec,
} from '../tonkit.js'

import * as nacl from 'tweetnacl'
import * as fs from 'fs'

const fsp = fs.promises

process.on('unhandledRejection', error => {
  console.error('unhandledRejection')
  console.error(error)
  process.exit(2)
})

const PATH_DATA = 'data'
const BASE = join(PATH_DATA, 'test')
const FN_SECKEY = BASE + '.sk'
const FN_HASH = BASE + '.hash'

const SCRIPT_TEST_SIGNING = 'fift/test-signing.fif'
const client = new Client

run()

async function run() {

  let kp, hash, result, sig

  await exec('rm -rf ' + PATH_DATA)
  await exec('mkdir ' + PATH_DATA)
  result = await client.fift.run(SCRIPT_TEST_SIGNING, 0, BASE)
  console.log(result)
  result = await fsp.readFile(FN_SECKEY)
  console.log(result)
  kp = nacl.sign.keyPair.fromSeed(result)
  console.log(kp)
  hash = await fsp.readFile(FN_HASH)
  console.log(hash)
  sig = nacl.sign.detached(hash, kp.secretKey)
  console.log(sig)
}